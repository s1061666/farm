<!--程式碼範例-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<title>KML圖層套疊</title>
<script type="text/javascript" src="https://api.tgos.tw/TGOS_API/tgos?ver=2&AppID=YTvBoU3+KlGNnQY0bVrcBWPZ6C7ghKJQsdnrE+DmFV/brGnGWAG39Q==&APIKey=cGEErDNy5yN/1fQ0vyTOZrghjE+jIU6ufThSqYm3hjzSaK8pYEgKKmkHcoqPE1UtE4fEanYtjmApOs5vhN+nxmCK7qBmwst6H58Hn7rqQDI7x0dHVM+h1Wchuje3Of208tsorv9DnMKDrL6/gu3ISGf/+J3cAuXvDJjIOrPMFxUaaFh2EiT5YxVwZioViKXseoUe6S9XFbA+I2qRj7ifJ8mCWkY0hwAiftTN+yzqflOZDkRsBbKbhW8GCjq9BTniO3WWghoHMOKLqr0P/RRMW10/lVQFh2X+Q8sNR22Ha6AhaLL51CaoVeWYDnyixQFgt5cblMNk9rNxbP5Hr1Cs17AB6kxnF1Is3ubau3d2Co7r99gmJSq0qQ==" charset="utf-8"></script>
<!--下載後請將yourID及yourkey取代為您申請所取得的APPID及APIKEY方能正確顯示服務-->
<script type="text/javascript"> 
	var pMap = null;
	var kmlLayer = null;
	function InitWnd() {
		var pOMap = document.getElementById("TGMap");
		var mapOptions = {
			mapTypeControl: false	//mapTypeControl(關閉地圖類型控制項)
		};
		pMap = new TGOS.TGOnlineMap(pOMap, TGOS.TGCoordSys.EPSG3857, mapOptions);	//宣告TGOnlineMap地圖物件並設定坐標系統
	}
	
	function AddKML() {
		if (kmlLayer) {
			 kmlLayer.removeKml();							//假如圖面上已經有KML疊加層, 則先移除掉現有的kml圖層再加入新圖層
		}
		var list = document.getElementById("urlList");
		var url = list.options[list.selectedIndex].text;	//取得下拉選單的KML網址
		kmlLayer = new TGOS.TGKmlLayer(url, {				//設定KML圖層
				map: pMap,									//設定欲疊加的底圖
				suppressInfoWindows: false,				//可點選KML圖徵顯示訊息視窗, 若設為true則無法顯示訊息視窗
				preserveViewport: true						//疊加KML後將圖面縮放至KML的範圍, 若設為false則取消這個功能
            });
	}
</script>            
</head>
<body style="margin:0px" onLoad="InitWnd();">
	<div id="TGMap" style="width:700px; height:600px; border: 1px solid #C0C0C0;"></div>
	<br/>
	<!--建立下拉選單, 提供預設兩組KML網址-->
	<select id="urlList">
		<option>https://api.tgos.tw/TGOS_API/wrarb.kml</option>
		<option>https://api.tgos.tw/TGOS_API/GWREGION.kml</option>
        <option>http://120.110.115.152/20191205.kml</option>
        <option>http://120.110.115.152/20191206.kml</option>
	</select>
	<br/>
	<input type="button" value="加入KML圖層" onClick="AddKML();">
</html>                            
