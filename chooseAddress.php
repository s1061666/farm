<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"><!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <title>農業風水師</title>
    <style>
            #up{
            background-color: #2A6041!important;
            }
    </style>
</head>
<body class="text-center">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
            <a class="navbar-brand" href="first.html"><<span class="h3 mx-1">地區選擇</span></a> <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="main.php">羅盤</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="reservation.php">預約生產單</a>
                </li>
              </ul>
          </div>
      </nav><br>


    <div class="jumbotron jumbotron-fluid">
      <div class="container">
      	<h2>請選擇要查看的地域</h2>
      </div>
    </div>
    <br>


    <div class="container">
      <?php

	include_once 'phpscripts/Manager.php';

	function setAddress($villageNo) {
		$manager = new AddressManager();
    		$location = explode('-', $villageNo);
		$manager->setAddressSession($location[0], $location[1], $location[2] );
    		print_r($_SESSION);
		header("Location:main.php");
	}

	function showAddress() {
		$manager = new AddressManager();
		$result = $manager->getAddressLists();

    		if (count($result) < 1) {
      			header("Location:reservation.php");
    		}

		echo '<form>';
		
		// Array ( [0] => Array ( [CityNo] => 10020 [TownNo] => 10020020 [VillageNo] => 10020020001 [Name] => 嘉義市西區香湖里 ) ) 
		for ($i = 0; $i < count($result); $i++) {
      		//echo '<input type="hidden" name="villageNo" value="'.$result[$i]['CityNo'].'"></input>';
			echo '<input type="radio" name="villageNo" required value="'.$result[$i]['CityNo'].'-'.$result[$i]['TownNo'].'-'.$result[$i]['VillageNo'].'"> '.$result[$i]['Name'].'</input><br>';
		}
    		echo '<br >';
		echo '<input type="submit">';
		echo '</form>';
	}

	if(isset($_GET['villageNo'])) {
	    setAddress ($_GET['villageNo']);
	} else {
		showAddress();
	}

?>
    </div>
    <!-- <input class="btn btn-outline-success" onclick="javascript:location.href='main.php'" type="button" value="回羅盤"></input>-->
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
  </script> 
  <script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
  </script> 
  <script crossorigin="anonymous" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js">
  </script>
  <hr />
</body>
</html>
