<?php session_start(); ?>
<html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script> <?php
// $dsn        = "mysql:host=localhost;dbname=hacktonfarm;charset=utf8";
// $username   = "dluser";
// $password   = "dluser@515";
// $connection = new PDO($dsn, $username, $password);
// $cropname   = $connection->query("SELECT cropname FROM cropcode");
  include_once "./connect_local_test.php";
?>
  <style>
  #up {
    background-color: #2A6041 !important;
  }

  #div {
    margin-bottom: 10px;
    display: flex;
    align-items: center;
  }

  #label {
    display: inline-block;
    width: 300px;
  }

  #input:invalid+span:after {
    content: '✖';
    padding-left: 5px;
  }

  #input:valid+span:after {
    content: '✓';
    padding-left: 5px;
  }

  #back {

    background-image: url('image/logo/fire-L.jpg');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;

    width: 100%;
    height: 100%;
  }
  </style>
  <style>
  .navbar-light .navbar-brand {
    color: #ffffff;
  }

  .navbar-light .navbar-nav .nav-link {
    color: rgb(255, 255, 255);
  }

  #up {
    background-color: #2A6041 !important;
  }

  .carousel {
    perspective: 500px;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-image: url(image/bg1.jpg) !important;
    background-repeat: no-repeat;
    background-position: center 20%;
  }

  .slider {
    width: 50%;
    margin: 100px auto;
  }

  .slick-slide img {
    margin: auto;
  }

  /* Arrows */
  .slick-prev,
  .slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    border: none;
    background: transparent;
  }

  .slick-prev:hover,
  .slick-prev:focus,
  .slick-next:hover,
  .slick-next:focus {
    color: transparent;
    outline: none;
    background: transparent;
  }

  .slick-prev:hover:before,
  .slick-prev:focus:before,
  .slick-next:hover:before,
  .slick-next:focus:before {
    opacity: 1;
  }

  .slick-prev.slick-disabled:before,
  .slick-next.slick-disabled:before {
    opacity: .25;
  }

  .slick-prev:before,
  .slick-next:before {
    font-family: 'slick';
    font-size: 20px;
    line-height: 1;
    opacity: .75;
    color: white;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .slick-prev {
    left: -25px;
  }

  [dir='rtl'] .slick-prev {
    right: -25px;
    left: auto;
  }

  .slick-prev:before {
    content: '←';
  }

  [dir='rtl'] .slick-prev:before {
    content: '→';
  }

  .slick-next {
    right: -25px;
  }

  [dir='rtl'] .slick-next {
    right: auto;
    left: -25px;
  }

  .slick-next:before {
    content: '→';
  }

  [dir='rtl'] .slick-next:before {
    content: '←';
  }
  </style>

<body class="text-center">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
      <a class="navbar-brand" href="first.html"> <span class="h3 mx-1">農業風水師</span></a> <button
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
        class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span
          class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="main_2.html">首頁<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="woodtest.php">預約生產單</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="moneytest1.php">金</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="earthcontroller.php">木</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="watertest.php">水</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="firetest.php">火</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="test1.php">土</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">登入</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="register.php">註冊</a>
          </li>
        </ul>
      </div>
    </nav><br>

    <body>
      <div class="container">
        <div class="row justify-content-center">
          <table class="table">
            <thead>
              <th scope="col" style=" text-align: center;">選擇預約日期</th>
            </thead>
            <tbody>
              <form method="post" action="producefarm.php">
                <td>
                  <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <lable>選擇要種植的作物:</lable>
                      <select class="form-control" name="cropname">
                        <?php
foreach ($cropname as $resultcrop) {
    print '<option value="' . $resultcrop['cropname'] . '">' . $resultcrop["cropname"] . '</option>';
}
?>
                      </select>
                      <lable>預計時間:</lable>
                      <input type="date" class="form-control" name="checkdate" value="checkdate">
                      <input type="radio" name="plantdate" value="plant" checked="true"> 種植<br>
                      <input type="radio" name="plantdate" value="harv"> 採收<br>
                      <button class="btn btn-primary">預約/查看時間</button>
                    </div>
                  </div>
                  <a href="farmer.php">建立農地</a>
                </td>
                </td>
            </tbody>
          </table>
        </div>
      </div>
    </body>

</html>