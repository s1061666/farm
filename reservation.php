<?php
	ob_start();
	$dsn = "mysql:host=localhost;dbname=tari";
	$username = "root";
	$password = "dluser@515";
	$connection = new PDO($dsn, $username, $password);
	$index = 7;
	$num = 1;
	//$index_date = (string)date("Y-m-d");
	//$date = date("Y-m-d", strtotime($_GET['except_value']));
	//$date = date_create(date("Y-m-d", strtotime($_GET['except_value'])));
	//$date = date("Y-m-d", strtotime($_GET['except_value']));
	if (isset($_GET['except_value'])) {
		$date = new Datetime($_GET['except_value']);
		$index_date = $date->modify("-1 year")->format("Y-m-d");
	} else {
		$index_date = (string)date("Y-m-d");
	}
	//$index_date = date_sub($date,date_interval_create_from_date_string("365 days"));

	if(isset($_GET['day'])){
		$index = $_GET['day'];
		if($index == 30){
			$num = 7;
		}
	}
	if(isset($_GET['datetime'])){
		$index_date = $_GET['datetime'];
	}
	$crop = 'W';
	$crop_name = '洋香瓜';
	if(isset($_GET['crop'])){
		$crop = $_GET['crop'];
		$crop_name = '小番茄';
	}
	
	include('feature_money.php');
	$data = get_data($connection,$index_date,$crop,$index);

	$date=date_create($index_date);

	$date->modify('-'.$index.' day');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/lib/jquery-1.8.3.min.js"></script>

	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.min.js"></script>
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.time.js"></script>    
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jshashtable-2.1.js"></script>    
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.numberformatter-1.2.3.min.js"></script>
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.symbol.js"></script>
	<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.axislabels.js"></script>
	
    <title>農業風水師</title>
	<script> 
		function get_date() {
			var data = document.getElementById('datetime').value;
			var day = document.getElementById('day').value;
			//$_GET['date'], $_GET['cropName'], $_GET['villageNo'], $_GET['cityNo'], $_GET['townNo']
			//document.location.href="?datetime="+data+"&day="+day + "&date="2018-05-31"&cropName="洋香瓜"&cityNo="10009"&townNo="1000911"&villageNo="1000911004"&submitted="預約並查看		}
			document.location.href="reservation.php?datetime="+data+"&day="+day + <?php echo ('"&date="+"'.$_GET['date'].'"+"&cropName="+"'.$_GET['cropName'].'"+"&cityNo="+'.$_GET['cityNo'].'+"&townNo="+'.$_GET['townNo'].'+"&villageNo="+'.$_GET['villageNo'].'+"&submitted="+"預約並查看"'); ?>;
		}
		
		var data2 = <?php echo $data[1];?>;
		var data3 = <?php echo $data[0];?>;
		
		var dataset = [
			{
				label: "交易總量",
				data: data3,         
				color: "#756600",
				bars: {
					show: true, 
					align: "center",
					barWidth: 24 * 60 * 60 * 600,
					lineWidth:1
				}
			}, {
				label: "交易均價",
				data: data2,
				yaxis: 2,
				color: "#0062FF",
				points: { symbol: "triangle", fillColor: "#0062FF", show: true },
				lines: {show:true}
			}
		];

			
		var options = {
			xaxis: {
				mode: "time",
				tickSize: [<?php print $num;?>, "day"],  
				tickLength: 0,
				axisLabel: "交易日期",
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial',
				axisLabelPadding: 10,
				color: "black"
			},
			yaxes: [{
					position: "left",
					max: 100,
					color: "black",
					axisLabel: "公噸",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Verdana, Arial',
					axisLabelPadding: 3            
				}, {
					position: "right",
					max: 100,
					clolor: "black",
					axisLabel: "新台幣",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Verdana, Arial',
					axisLabelPadding: 3            
				}
			],
			legend: {
				noColumns: 1,
				labelBoxBorderColor: "#000000",
				position: "nw"        
			},
			grid: {
				hoverable: true,
				borderWidth: 3,        
				backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
			}
		};

		$(document).ready(function () {
			$.plot($("#flot-placeholder"), dataset, options);
			$("#flot-placeholder").UseTooltip();
		});




		function gd(year, month, day) {
			return new Date(year, month - 1, day).getTime();
		}

		var previousPoint = null, previousLabel = null;

		$.fn.UseTooltip = function () {
			$(this).bind("plothover", function (event, pos, item) {
				if (item) {
					if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
						previousPoint = item.dataIndex;
						previousLabel = item.series.label;
						$("#tooltip").remove();
						//console.log(item.datapoint);
						var x = item.datapoint[0];
						var y = item.datapoint[1];

						var color = item.series.color;
						//var date = "Jan " + new Date(x).getDate();
						var date = new Date(x).getFullYear()+"-"+(new Date(x).getMonth()+1)+"-"+new Date(x).getDate();

						//console.log(item);
						var unit = "";

						if (item.series.label == "交易總量") {
							unit = "公噸";
						} else if (item.series.label == "交易均價") {
							unit = "新台幣";
						}

						showTooltip(item.pageX, item.pageY, color,
									"<strong>" + item.series.label + "</strong><br>" + date +
									" : <strong>" + y + "</strong> " + unit + "");
					}
				} else {
					$("#tooltip").remove();
					previousPoint = null;
				}
			});
		};

		function showTooltip(x, y, color, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y - 40,
				left: x - 120,
				border: '2px solid ' + color,
				padding: '3px',
				'font-size': '9px',
				'border-radius': '5px',
				'background-color': '#fff',
				'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
				opacity: 0.9
			}).appendTo("body").fadeIn(200);
		}
	</script> 
  </head>
	<style>
		#up{
			background-color: #2A6041!important;
		}
		#div {
		   margin-bottom: 10px;
		   display: flex;
		   align-items: center;
		}

		#label {
		   display: inline-block;
		   width: 300px;
		}

		#input:invalid+span:after {
		   content: '✖';
		   padding-left: 5px;
		}

		#input:valid+span:after {
		   content: '✓';
		   padding-left: 5px;
		}
    </style>
  <body class="text-center" id='table'>
        <div class="container">
            <!--nav bar-->
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
						<a class="navbar-brand" href="first.html"><span class="h3 mx-1">預約單</span></a> <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="main.php">羅盤</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="reservation.php">預約生產單</a>
                </li>
              </ul>
					  </div>
				  </nav>
            <br>
	</div>
	<div class="container">
		<h2>預約生產單</h2>
		<hr>
		<?php

	include_once 'phpscripts/Manager.php';
		        
		        function base_show() {
		        	$address = new AddressManager();
			        echo '
			        	<form action="reservation.php" method="get">
			        		<div>
			        			<label for="date">種植日期:</label> <input id="date" name="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required="" type="date" min="'.date("Y-m-d").'"> <span class="validity"></span>
			        		</div>
			        		<p id=except_date></p>
			        		<input id="except_value" name="except_value" type="hidden" value="0">

			        		<!--
			        		<div>
			        			<label for="cropName">作物名稱:</label> <input id="cropName" name="cropName" required="required" type="text"> <span class="validity"></span>
			        		</div>
			        		-->
			        		<div>
			        			<label for="cropName">作物名稱:</label> 
			        				<select id="cropName" name="cropName"> <span class="validity"></span>
			        					<option>洋香瓜</option>
			        					<option>小番茄</option>
			        				</select>
			        		</div>

			        			<select id="city" name="cityNo"> <span class="validity"></span>
			        				<option>選擇縣市</option>';
			        					echo $address->getCityOpt();
			        echo '
			        			</select>
			        			
			        			<select id="town" name="townNo"> <span class="validity"></span>
			        				<option>選擇鄉鎮</option>
			        			</select>
			        			

			        			<select id="village" name="villageNo"> <span class="validity"></span>
			        				<option>選擇村里</option>';
			        					#echo $address->getVillageOpt();
			        echo '
			        			</select>

			        		<hr>
			        		<input class="btn btn-outline-success" onclick="history.back()" type="button" value="回到上一頁"> <input class="btn btn-outline-success" name="submitted" type="submit" value="預約並查看"> 
			        	</form>';

			    }

		        function run($_date, $_cropName, $_villageNo , $_cityNo, $_townNo) {
		        	$manager = new AddressManager();
		        	$result  = $manager->getAddressName($_villageNo);

		        	echo '<h3>土壤資訊</h3>';
		        	echo '<iframe src="http://maps.google.co.jp/maps?q=' . $result . '&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="100%" height="100%"></iframe>';

		        	echo '<hr />';

		        	$ph    = $manager->getPHInfo($_villageNo);
		        	$flair = $manager->getFlairInfo($_villageNo);

		        	echo '<h4>' . $ph . '</h4>';
		        	echo '<h4>' . $flair . '</h4>';

		        	echo '<hr />';
        $dsn = "mysql:host=localhost;dbname=tari;charset=utf8";
        $username = "root";
        $password = "dluser@515";
        try{
                $connection = new PDO($dsn, $username, $password);
                echo "You have connected.";

		$index = 7;
		$num = 1;
		if (isset($_GET['except_value'])) {
			$date = new Datetime($_GET['except_value']);
			$index_date = $date->modify("-1 year")->format("Y-m-d");
		} else {
			$index_date = (string)date("Y-m-d");
		}
		if(isset($_GET['day'])){
			$index = $_GET['day'];
			if($index == 30){
				$num = 7;
			}
		}
		if(isset($_GET['datetime'])){
			$index_date = $_GET['datetime'];
		}
		$crop = 'W';
		$crop_name = '洋香瓜';
		if(isset($_GET['crop'])){
			$crop = $_GET['crop'];
			$crop_name = '小番茄';
		}

		//include('feature_money.php');
		$data = get_data($connection,$index_date,$crop,$index);
		$date=date_create($index_date);
		$date->modify('-'.$index.' day');
	}
	catch(PDOException $e){
		$error_message = $e->getMessage();
                echo $error_message;
	}		
		       ?>
			<h5 class='no-margin m-b'>請選擇日期</h5>
			<input type="date" id="datetime" class="form-control" onchange="get_date();" value="<?php print $index_date;?>" required autofocus>
			<h5 class='no-margin m-b'>請選擇顯示天數</h5>
			<select id="day" name="day" class="form-control" onchange="get_date();" value="2">
				<option value="7" <?php if($index == 7){print 'selected';}?>>一週趨勢</option>
				<option value="30" <?php if($index == 30){print 'selected';}?>>一月趨勢</option>
			</select>
            <hr>
            <br>
            <div class="container">
                <div class="row">
					<div class='col-sm-12 col-md-12'>
						<div class="panel panel-card">
							<div class="panel-title">
								<h4 class='no-margin m-b'><?php print date_format($date,"Y-m-d").' ~ '.$index_date.'天交易市場統計';?></h4>
							</div>
							<div class="panel-body">
								<div id="flot-placeholder" style="width:100%;height:300px;margin:0 auto"></div>
							</div>
						</div>
					</div>
                </div>
            </div>		        	
<?php

		        	//echo '<iframe src="http://120.110.112.32/templated-industrious/web/serve/money.php" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="100%" height="600"></iframe>';

		        	echo '
		        		<form action="reservation.php" method="get">
			        		<div>
			        			<input type="hidden" id="input" name="date" value=' . $_date . ' pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required="" type="date"> <span class="validity"></span>
			        		</div>


			        		<div>
			        			<input type="hidden" id="input" name="cropName" value=' . $_cropName . ' required="required" type="text"> <span class="validity"></span>
			        		</div>

			        		<div>
			        			<input type="hidden" id="input" name="villageNo" value=' . $_villageNo . ' pattern="[0-9]{10}" required="required" type="text"> <span class="validity"></span>
			        		</div>

			        		<div>
			        			<input type="hidden" id="input" name="cityNo" value=' . $_cityNo . ' required="required" type="text"> <span class="validity"></span>
			        		</div>

			        		<div>
			        			<input type="hidden" id="input" name="townNo" value=' . $_townNo . ' required="required" type="text"> <span class="validity"></span>
			        		</div>

			        		<div>
			        			<input type="hidden" id="input" name="next" value=1 type="text"> <span class="validity"></span>
			        		</div>

			        		<hr>
			        		<input class="btn btn-outline-success" onclick="history.back()" type="button" value="回到上一頁"> <input class="btn btn-outline-success" name="submitted" type="submit" value="建立"> 
		        		</form>';


		        /*

		        */
		        #header("location.replace('main.php')";
		        }

		        function nextone($_date, $_cropName, $_villageNo, $_cityNo, $_townNo) {

			        $listController = new ListController();
			        $year           = date('Y', strtotime($_GET['date']));
			        $month          = date('m', strtotime($_GET['date']));
			        $day            = date('d', strtotime($_GET['date']));
			        $cropName       = $_cropName;
			        //echo $year . $month . $day . $cropName . $_villageNo;
			        $listController->createNewProductList($day, $month, $year, $cropName, $_villageNo);
			        $manager = new AddressManager();
			        $manager->setAddressSession($_cityNo, $_townNo, $_villageNo);
			        
			       header("Location:wood.php");
			       ob_end_flush();
			    }

			        if (isset($_GET['date']) && isset($_GET['cropName']) && isset($_GET['villageNo']) && isset($_GET['cityNo']) && isset($_GET['townNo']) && !isset($_GET['next'])) {
			        	run($_GET['date'], $_GET['cropName'], $_GET['villageNo'], $_GET['cityNo'], $_GET['townNo']);

			        } elseif (isset($_GET['date']) && isset($_GET['cropName']) && isset($_GET['villageNo']) && isset($_GET['cityNo']) && isset($_GET['townNo']) && isset($_GET['next'])) {
			        	nextone($_GET['date'], $_GET['cropName'], $_GET['villageNo'], $_GET['cityNo'], $_GET['townNo']);
			        } else {
			        	base_show();
			        	echo '	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
	</script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
	</script> 
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js">
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
	</script>';
			        }
		        ?>
	</div><!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->

	<script>
		$(document).on('change', '#city', function(){
		   var city = $('#city :selected').val();//注意:selected前面有個空格！

   			$.ajax({
      			url:"phpscripts/Manager.php",				
      			method:"POST",
      			data:{
         			city: city
      			},					
      			success:function(res){	
      				//alert(res);
      				$('#village').html('<option>選擇村里</option>');
         			$('#town').html(res);
      			}

   			});//end ajax
		});

		$(document).on('change', '#town', function(){
		   var town = $('#town :selected').val();//注意:selected前面有個空格！
   			$.ajax({
      			url:"phpscripts/Manager.php",				
      			method:"POST",
      			data:{
         			town: town
      			},					
      			success:function(res){	
      				//alert(res);

         			 $('#village').html(res);
      			}

   			});//end ajax
		});

		$(document).on('change', '#date', function(){
		   var date = $('#date').val();//注意:selected前面有個空格！
   			$.ajax({
      			url:"phpscripts/Manager.php",				
      			method:"POST",
      			data:{
         			reservation_date: date
      			},					
      			success:function(res){	
         			 $('#except_date').html("預計收穫時間："+res);
         			 $('#except_value').val(res);
      			}

   			});//end ajax
		});
	</script>
			<!--
		<script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> 
		<script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> 
		-->
	<script crossorigin="anonymous" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</body>
</html>
