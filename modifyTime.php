<!DOCTYPE html>

<html lang="en">
<head>
  <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet"><!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"><!-- Bootstrap CSS -->
  <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" rel="stylesheet">

  <title>農業風水師</title>
  <style>
         #up{
         background-color: #2A6041!important;
         }
  </style>
</head>

<body class="text-center">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
            <a class="navbar-brand" href="first.html"><span class="h3 mx-1"><木>日期修改</span></a> <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="main.php">羅盤</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="reservation.php">預約生產單</a>
                </li>
              </ul>
          </div>
      </nav><br>


    <div class="jumbotron jumbotron-fluid"  >
      <p>修改實際時間</p>
    </div>

    <hr>


    <div class="container">
      <?php
        include_once("phpscripts/Manager.php");

        function display($seasonControll) {
          $info = $seasonControll->getInfo($_GET['seqID']);
          $remind = $seasonControll->getRemind($_GET['seqID']);
          $time = $seasonControll->getDetailTime($_GET['seqID']);
          showForm($info, $remind, $time);
        }

        function showForm($info, $remind, $time) {
          echo '<h2>'.$info['cropName'].'</h2>';
          echo '<form method="get" action="modifyTime.php">';
          echo '<input type="hidden" name="sd_SID" value="'.$_GET['sd_SID'].'">';
          echo '<input type="hidden" name="seqID" value="'.$_GET['seqID'].'">';
          echo '<input type="hidden" name="season" value="'.$_GET['season'].'">';
          echo '<h4>'.$_GET["periodName"].'</h4>';
          echo '<h4>'.$_GET["title"].'</h4>';
          echo '<label for="StartDateTime">預計開始時間：</label>'.$time['E_StartDateTime'].'<br>';
          echo '<label for="EndDateTime">預計結束時間：</label>'.$time['E_EndDateTime'].'<br>';
          echo '<hr />';
          echo '<label for="StartDateTime">實際開始時間：</label>'.$time['R_StartDateTime'].'<br>';
          echo '<label for="EndDateTime">實際結束時間：</label>'.$time['R_EndDateTime'].'<br>';
          echo '<label for="SeasonNo">種植季節：</label>'.$info['seasonNo'].'<br>';
          echo '<label for="VillageName">農地位址：</label>'.$info['villageName'].'<br>';
          echo '<hr />';
          echo '<label for="editSTime">修改開始時間為：</label><input type=date id="editSTime" name="editSTime" >';
          echo '<label for="editETime">修改結束時間為：</label><input type=date id="editETime" name="editETime" >';
          echo '<br>';
          echo '<br>';
          echo '<input type ="button" onclick="history.back()" value="取消"></input>';
          echo '<input type="submit" name="submitted" value="修改">';
          //echo '<label for="CurrentSeqID">當前狀態：</label>'.$result[$i]['CurrentSeqID'].'<br>';
          //echo '<tr> <td> <button onclick="editTable('.($i + 1).')">修改</button> <td> <tr>';
          echo '</table>';
          echo '</form>';
        }

        function editTime($seasonControll) {
          $seasonControll->modifyDate($_GET['editSTime'], $_GET['seqID'], $_GET['season']);
          $info = $seasonControll->getInfo($_GET['seqID']);
          header('Location:wood.php');
        }

        if(isset($_GET['sd_SID']) && isset($_GET['seqID']) && isset($_GET['title']) && isset($_GET['periodName']) && isset($_GET['season'])) {
          $seasonControll = new SeasonControll($_GET['sd_SID']);
          display($seasonControll);
        }
        if (isset($_GET['sd_SID']) && isset($_GET['seqID']) && isset($_GET['editSTime']) && isset($_GET['season'])) {
          $seasonControll = new SeasonControll($_GET['sd_SID']);
            editTime($seasonControll);
        } 
      ?>
    </div>
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
  </script> 
  <script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
  </script> 
  <script crossorigin="anonymous" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js">
  </script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
  </script>
  <script>
    $(document).on('change', '#editSTime', function(){
       var editSTime = $('#editSTime').val();//注意:selected前面有個空格！

        $.ajax({
            url:"phpscripts/Manager.php",       
            method:"POST",
            data:{
              reservation_date2: editSTime,
              type: 'add'
            },          
            success:function(res){  
               $('#editETime').val(res);
            }

        });//end ajax
    });

    $(document).on('change', '#editETime', function(){
       var editETime = $('#editETime').val();//注意:selected前面有個空格！

        $.ajax({
            url:"phpscripts/Manager.php",       
            method:"POST",
            data:{
              reservation_date2: editETime,
              type: 'sub'
            },          
            success:function(res){  
               $('#editSTime').val(res);
            }

        });//end ajax
    });
  </script>

</body>
</html>