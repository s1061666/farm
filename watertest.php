<?php session_start(); ?>
<html>
<title>
  農業風水師-水測試
</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<?php
// $dsn        = "mysql:host=localhost;dbname=hacktonfarm;charset=utf8";
// $username   = "dluser";
// $password   = "dluser@515";
// $connection = new PDO($dsn, $username, $password);
include_once "./connect_local_test.php";
$num        = date("i");
$num_h      = date("H");
$rain_data2 = array();
if ($num <= 10) {
    $tmp_h = $num_h;
    $num_h = $tmp_h - 1;
    $num   = 50;
} elseif ($num > 10 && $num % 10 != 0) {
    if ($num <= 20) {
        $tmp = $num;
        $num = $tmp - ($num % 10) - 10;
    }
    $tmp = $num;
    $num = $tmp - ($num % 10) - 20;
} elseif ($num > 10 && $num % 10 == 0) {
    $tmp0 = $num;
    $num  = $tmp0 - 20;
}

if ($num == '0') {
    $num = "00";
}
/*if(isset($_POST["testid"])){
$id=$_POST["testid"];
}*/
$sheet2 = $con->query("SELECT * FROM test_plant WHERE id='" . $_SESSION["earthid"] . "'");
foreach ($sheet2 as $sheet3) {
    $city = $sheet3["city"];
    $town = $sheet3["town"];
}
$dataimg  = "https://www.cwb.gov.tw/Data/radar/CV1_3600_" . (string)date("Ymd") . ($num_h) . ($num) . '.png';
$dataimg1 = "https://www.cwb.gov.tw/Data/satellite/LCC_IR1_CR_2750/LCC_IR1_CR_2750-" . (string)date("Y-m-d") . ("-") . ($num_h) . ("-") . ($num) . '.jpg';
if ($city == "宜蘭縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-003?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "桃園市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-007?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "新竹縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-011?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "苗栗縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-015?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "彰化縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-019?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "南投縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-023?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "雲林縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-027?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "嘉義縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-031?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "屏東縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-035?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "臺東縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-039?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "花蓮縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-043?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "澎湖縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-047?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "基隆市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-051?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "新竹市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-055?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "嘉義市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-059?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "臺北市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-063?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "高雄市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-067?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "新北市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-071?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "臺中市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-075?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "臺南市") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-079?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "連江縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-083?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
} elseif ($city == "金門縣") {
    $json_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-087?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C');
    $data        = json_decode($json_string, true);
}
$json_farm      = file_get_contents("F-A0010-001.json");
$data_farm      = json_decode($json_farm, true);
$farmo          = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][0]["dataDate"];
$farmt          = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][1]["dataDate"];
$farmth         = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][2]["dataDate"];
$farmfr         = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][3]["dataDate"];
$farmf          = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][4]["dataDate"];
$farms          = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][5]["dataDate"];
$farmsv         = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherForecasts"]["location"][0]["weatherElements"]["Wx"]["daily"][6]["dataDate"];
$farmnorth      = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][0];
$farmmiddle     = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][1];
$farmsouth      = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][2];
$farmnortheast  = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][3];
$farmeast       = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][4];
$farmsourtheast = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["agrAdvices"]["agrForecasts"]["location"][5];

$notices = $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherProfile"];

$farmnorthdo    = $farmnorth["weatherElements"]["daily"][0]["degreeDay"];
$farmnorthdt    = $farmnorth["weatherElements"]["daily"][1]["degreeDay"];
$farmnorthdth   = $farmnorth["weatherElements"]["daily"][2]["degreeDay"];
$farmnorthdfr   = $farmnorth["weatherElements"]["daily"][3]["degreeDay"];
$farmnorthdf    = $farmnorth["weatherElements"]["daily"][4]["degreeDay"];
$farmnorthds    = $farmnorth["weatherElements"]["daily"][5]["degreeDay"];
$farmnorthdsv   = $farmnorth["weatherElements"]["daily"][6]["degreeDay"];

$farmnorthao  = $farmnorth["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmnorthat  = $farmnorth["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmnorthath = $farmnorth["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmnorthafr = $farmnorth["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmnorthaf  = $farmnorth["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmnorthas  = $farmnorth["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmnorthasv = $farmnorth["weatherElements"]["daily"][6]["accumulatedTemperature"];

$farmmiddledo  = $farmmiddle["weatherElements"]["daily"][0]["degreeDay"];
$farmmiddledt  = $farmmiddle["weatherElements"]["daily"][1]["degreeDay"];
$farmmiddledth = $farmmiddle["weatherElements"]["daily"][2]["degreeDay"];
$farmmiddledfr = $farmmiddle["weatherElements"]["daily"][3]["degreeDay"];
$farmmiddledf  = $farmmiddle["weatherElements"]["daily"][4]["degreeDay"];
$farmmiddleds  = $farmmiddle["weatherElements"]["daily"][5]["degreeDay"];
$farmmiddledsv = $farmmiddle["weatherElements"]["daily"][6]["degreeDay"];

$farmmiddleao  = $farmmiddle["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmmiddleat  = $farmmiddle["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmmiddleath = $farmmiddle["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmmiddleafr = $farmmiddle["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmmiddleaf  = $farmmiddle["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmmiddleas  = $farmmiddle["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmmiddleasv = $farmmiddle["weatherElements"]["daily"][6]["accumulatedTemperature"];

$farmsouthdo  = $farmsouth["weatherElements"]["daily"][0]["degreeDay"];
$farmsouthdt  = $farmsouth["weatherElements"]["daily"][1]["degreeDay"];
$farmsouthdth = $farmsouth["weatherElements"]["daily"][2]["degreeDay"];
$farmsouthdfr = $farmsouth["weatherElements"]["daily"][3]["degreeDay"];
$farmsouthdf  = $farmsouth["weatherElements"]["daily"][4]["degreeDay"];
$farmsouthds  = $farmsouth["weatherElements"]["daily"][5]["degreeDay"];
$farmsouthdsv = $farmsouth["weatherElements"]["daily"][6]["degreeDay"];

$farmsouthao  = $farmsouth["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmsouthat  = $farmsouth["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmsouthath = $farmsouth["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmsouthafr = $farmsouth["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmsouthaf  = $farmsouth["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmsouthas  = $farmsouth["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmsouthasv = $farmsouth["weatherElements"]["daily"][6]["accumulatedTemperature"];

$farmsoutheastdo  = $farmsourtheast["weatherElements"]["daily"][0]["degreeDay"];
$farmsoutheastdt  = $farmsourtheast["weatherElements"]["daily"][1]["degreeDay"];
$farmsoutheastdth = $farmsourtheast["weatherElements"]["daily"][2]["degreeDay"];
$farmsoutheastdfr = $farmsourtheast["weatherElements"]["daily"][3]["degreeDay"];
$farmsoutheastdf  = $farmsourtheast["weatherElements"]["daily"][4]["degreeDay"];
$farmsoutheastds  = $farmsourtheast["weatherElements"]["daily"][5]["degreeDay"];
$farmsoutheastdsv = $farmsourtheast["weatherElements"]["daily"][6]["degreeDay"];

$farmsoutheastao  = $farmsourtheast["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmsoutheastat  = $farmsourtheast["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmsoutheastath = $farmsourtheast["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmsoutheastafr = $farmsourtheast["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmsoutheastaf  = $farmsourtheast["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmsoutheastas  = $farmsourtheast["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmsoutheastasv = $farmsourtheast["weatherElements"]["daily"][6]["accumulatedTemperature"];

$farmeastdo  = $farmeast["weatherElements"]["daily"][0]["degreeDay"];
$farmeastdt  = $farmeast["weatherElements"]["daily"][1]["degreeDay"];
$farmeastdth = $farmeast["weatherElements"]["daily"][2]["degreeDay"];
$farmeastdfr = $farmeast["weatherElements"]["daily"][3]["degreeDay"];
$farmeastdf  = $farmeast["weatherElements"]["daily"][4]["degreeDay"];
$farmeastds  = $farmeast["weatherElements"]["daily"][5]["degreeDay"];
$farmeastdsv = $farmeast["weatherElements"]["daily"][6]["degreeDay"];

$farmeastao  = $farmeast["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmeastat  = $farmeast["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmeastath = $farmeast["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmeastafr = $farmeast["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmeastaf  = $farmeast["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmeastas  = $farmeast["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmeastasv = $farmeast["weatherElements"]["daily"][6]["accumulatedTemperature"];

$farmnortheastdo  = $farmnortheast["weatherElements"]["daily"][0]["degreeDay"];
$farmnortheastdt  = $farmnortheast["weatherElements"]["daily"][1]["degreeDay"];
$farmnortheastdth = $farmnortheast["weatherElements"]["daily"][2]["degreeDay"];
$farmnortheastdfr = $farmnortheast["weatherElements"]["daily"][3]["degreeDay"];
$farmnortheastdf  = $farmnortheast["weatherElements"]["daily"][4]["degreeDay"];
$farmnortheastds  = $farmnortheast["weatherElements"]["daily"][5]["degreeDay"];
$farmnortheastdsv = $farmnortheast["weatherElements"]["daily"][6]["degreeDay"];

$farmnortheastao  = $farmnortheast["weatherElements"]["daily"][0]["accumulatedTemperature"];
$farmnortheastat  = $farmnortheast["weatherElements"]["daily"][1]["accumulatedTemperature"];
$farmnortheastath = $farmnortheast["weatherElements"]["daily"][2]["accumulatedTemperature"];
$farmnortheastafr = $farmnortheast["weatherElements"]["daily"][3]["accumulatedTemperature"];
$farmnortheastaf  = $farmnortheast["weatherElements"]["daily"][4]["accumulatedTemperature"];
$farmnortheastas  = $farmnortheast["weatherElements"]["daily"][5]["accumulatedTemperature"];
$farmnortheastasv = $farmnortheast["weatherElements"]["daily"][6]["accumulatedTemperature"];
if (empty($resultlocation)) {
} else {
    $weathermino  = $nowweather[0][2]["time"][0]["parameter"]["parameterName"];
    $weathermint  = $nowweather[0][2]["time"][1]["parameter"]["parameterName"];
    $weatherminth = $nowweather[0][2]["time"][2]["parameter"]["parameterName"];
    $weathermaxo  = $nowweather[0][4]["time"][0]["parameter"]["parameterName"];
    $weathermaxt  = $nowweather[0][4]["time"][1]["parameter"]["parameterName"];
    $weathermaxth = $nowweather[0][4]["time"][2]["parameter"]["parameterName"];
}
$rain_string = file_get_contents('https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0002-001?Authorization=CWB-9CEF63BE-6CE7-4510-B908-A7304D87277C&elementName=NOW');
$rain_data   = json_decode($rain_string, true);
//print_r($rain_data["records"]["location"][0]["parameter"][2]["parameterValue"] );
//817 ?>

<style>
.navbar-light .navbar-brand {
  color: #ffffff;
}

.navbar-light .navbar-nav .nav-link {
  color: rgb(255, 255, 255);
}

#up {
  background-color: #2A6041 !important;
}

.carousel {
  perspective: 500px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url(image/bg1.jpg) !important;
  background-repeat: no-repeat;
  background-position: center 20%;
}

.slider {
  width: 50%;
  margin: 100px auto;
}

.slick-slide img {
  margin: auto;
}

/* Arrows */
.slick-prev,
.slick-next {
  font-size: 0;
  line-height: 0;
  position: absolute;
  border: none;
  background: transparent;
}

.slick-prev:hover,
.slick-prev:focus,
.slick-next:hover,
.slick-next:focus {
  color: transparent;
  outline: none;
  background: transparent;
}

.slick-prev:hover:before,
.slick-prev:focus:before,
.slick-next:hover:before,
.slick-next:focus:before {
  opacity: 1;
}

.slick-prev.slick-disabled:before,
.slick-next.slick-disabled:before {
  opacity: .25;
}

.slick-prev:before,
.slick-next:before {
  font-family: 'slick';
  font-size: 20px;
  line-height: 1;
  opacity: .75;
  color: white;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.slick-prev {
  left: -25px;
}

[dir='rtl'] .slick-prev {
  right: -25px;
  left: auto;
}

.slick-prev:before {
  content: '←';
}

[dir='rtl'] .slick-prev:before {
  content: '→';
}

.slick-next {
  right: -25px;
}

[dir='rtl'] .slick-next {
  right: auto;
  left: -25px;
}

.slick-next:before {
  content: '→';
}

[dir='rtl'] .slick-next:before {
  content: '←';
}
</style>

<body class="text-center">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
      <a class="navbar-brand" href="first.html"><span class="h3 mx-1">農業風水師</span></a> <button
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
        class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span
          class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="main_2.html">首頁<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="woodtest.php">預約生產單</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="earthcontroller.php">木</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">登入</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="register.php">註冊</a>
          </li>
        </ul>

      </div>
    </nav><br>
    <div class="jumbotron jumbotron-fluid"
      style="background-image:url('image/water_bg.jpg'); background-size: 100%; background-repeat: no-repeat" ;>
      <div class="container">
        <p class="lead" style="color: red;font-weight:bold;">天氣資訊提醒</p>
        <p id="message" class="lead">NONE</p>
      </div>
    </div>
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <!--<form method="post">

			<select class="form-control" name="testid">
                         /*<?php
$sheet = $con->query("SELECT * FROM test_plant WHERE farmer_id='" . $_SESSION['log']['id'] . "'");
foreach ($sheet as $sheet1) {
    print("<option value='" . $sheet1["id"] . "'>" . $sheet1["farm_name"] . "</option>");
}
?>*/
                </select>
                <input type="submit" value="查詢">
		</form>-->
          <h4>天氣提醒:<?php echo $notices ?></h4>
        </div>
        <div class="col-md-12">
          <table class="table">
            <?php
for ($i = 0; $i < 818; $i++) {
    if ($rain_data["records"]["location"][$i]["parameter"][2]["parameterValue"] == $town) {
        $rain_now = "<p>目前累積降雨量" . $rain_data["records"]["location"][$i]["weatherElement"][0]["elementValue"] . "</p><br>";
        print($rain_now);
    }
}
foreach ($data as $data1) {
    for ($i = 0; $i < 30; $i++) {
        if (isset($data1["locations"][0]["location"][$i]["locationName"]) && $data1["locations"][0]["location"][$i]["locationName"] == $town) {
            print("<br>");
            print($data1["locations"][0]["location"][$i]["locationName"]);
            print("<thead><tr>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][0]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][3]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][5]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][7]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][9]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][11]["startTime"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][13]["startTime"] . "</td>");
            print("</tr></thead>");
            print("<tbody><tr>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][0]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][3]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][5]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][7]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][9]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][11]["elementValue"][0]["value"] . "</td>");
            print("<td>" . $data1["locations"][0]["location"][$i]["weatherElement"][10]["time"][13]["elementValue"][0]["value"] . "</td>");
            //print("</tr></tbody>");

        }
    }
}
print("<h4 style='color:red;'>" . $data_farm["cwbdata"]["resources"]["resource"]["data"]["agrWeatherForecasts"]["weatherProfile"] . "</h4>");
?>
          </table>
          <table class="table">
            <thead>
              <tr>
                <td>地區</td>
                <td></td>
                <td><?php echo $farmo ?></td>
                <td><?php echo $farmt ?></td>
                <td><?php echo $farmth ?></td>
                <td><?php echo $farmfr ?></td>
                <td><?php echo $farmf ?></td>
                <td><?php echo $farms ?></td>
                <td><?php echo $farmsv ?></td>

              </tr>
            </thead>
            <tbody>
              <tr>
                <td rowspan="2">北部地區</td>
                <td>度日</td>
                <td><?php echo $farmnorthdo ?></td>
                <td><?php echo $farmnorthdt ?></td>
                <td><?php echo $farmnorthdth ?></td>
                <td><?php echo $farmnorthdfr ?></td>
                <td><?php echo $farmnorthdf ?></td>
                <td><?php echo $farmnorthds ?></td>
                <td><?php echo $farmnorthdsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmnorthao ?></td>
                <td><?php echo $farmnorthat ?></td>
                <td><?php echo $farmnorthath ?></td>
                <td><?php echo $farmnorthafr ?></td>
                <td><?php echo $farmnorthaf ?></td>
                <td><?php echo $farmnorthas ?></td>
                <td><?php echo $farmnorthasv ?></td>
              </tr>
              </tr>
              <tr>
              <tr>
                <td rowspan="2">中部地區</td>
                <td>度日</td>
                <td><?php echo $farmmiddledo ?></td>
                <td><?php echo $farmmiddledt ?></td>
                <td><?php echo $farmmiddledth ?></td>
                <td><?php echo $farmmiddledfr ?></td>
                <td><?php echo $farmmiddledf ?></td>
                <td><?php echo $farmmiddleds ?></td>
                <td><?php echo $farmmiddledsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmmiddleao ?></td>
                <td><?php echo $farmmiddleat ?></td>
                <td><?php echo $farmmiddleath ?></td>
                <td><?php echo $farmmiddleafr ?></td>
                <td><?php echo $farmmiddleaf ?></td>
                <td><?php echo $farmmiddleas ?></td>
                <td><?php echo $farmmiddleasv ?></td>

              </tr>

              </tr>
              </tr>
              <tr>
                <td rowspan="2">南部地區</td>
                <td>度日</td>
                <td><?php echo $farmsouthdo ?></td>
                <td><?php echo $farmsouthdt ?></td>
                <td><?php echo $farmsouthdth ?></td>
                <td><?php echo $farmsouthdfr ?></td>
                <td><?php echo $farmsouthdf ?></td>
                <td><?php echo $farmsouthds ?></td>
                <td><?php echo $farmsouthdsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmsouthao ?></td>
                <td><?php echo $farmsouthat ?></td>
                <td><?php echo $farmsouthath ?></td>
                <td><?php echo $farmsouthafr ?></td>
                <td><?php echo $farmsouthaf ?></td>
                <td><?php echo $farmsouthas ?></td>
                <td><?php echo $farmsouthasv ?></td>

              </tr>


              </tr>
              <tr>
                <td rowspan="2">東北部地區</td>
                <td>度日</td>
                <td><?php echo $farmnortheastdo ?></td>
                <td><?php echo $farmnortheastdt ?></td>
                <td><?php echo $farmnortheastdth ?></td>
                <td><?php echo $farmnortheastdfr ?></td>
                <td><?php echo $farmnortheastdf ?></td>
                <td><?php echo $farmnortheastds ?></td>
                <td><?php echo $farmnortheastdsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmnortheastao ?></td>
                <td><?php echo $farmnortheastat ?></td>
                <td><?php echo $farmnortheastath ?></td>
                <td><?php echo $farmnortheastafr ?></td>
                <td><?php echo $farmnortheastaf ?></td>
                <td><?php echo $farmnortheastas ?></td>
                <td><?php echo $farmnortheastasv ?></td>

              </tr>


              </tr>
              <tr>
                <td rowspan="2">東部地區</td>
                <td>度日</td>
                <td><?php echo $farmeastdo ?></td>
                <td><?php echo $farmeastdt ?></td>
                <td><?php echo $farmeastdth ?></td>
                <td><?php echo $farmeastdfr ?></td>
                <td><?php echo $farmeastdf ?></td>
                <td><?php echo $farmeastds ?></td>
                <td><?php echo $farmeastdsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmeastao ?></td>
                <td><?php echo $farmeastat ?></td>
                <td><?php echo $farmeastath ?></td>
                <td><?php echo $farmeastafr ?></td>
                <td><?php echo $farmeastaf ?></td>
                <td><?php echo $farmeastas ?></td>
                <td><?php echo $farmeastasv ?></td>

              </tr>


              </tr>
              <tr>
                <td rowspan="2">東南部地區</td>
                <td>度日</td>
                <td><?php echo $farmsoutheastdo ?></td>
                <td><?php echo $farmsoutheastdt ?></td>
                <td><?php echo $farmsoutheastdth ?></td>
                <td><?php echo $farmsoutheastdfr ?></td>
                <td><?php echo $farmsoutheastdf ?></td>
                <td><?php echo $farmsoutheastds ?></td>
                <td><?php echo $farmsoutheastdsv ?></td>

              <tr>
                <td>積溫</td>
                <td><?php echo $farmsoutheastao ?></td>
                <td><?php echo $farmsoutheastat ?></td>
                <td><?php echo $farmsoutheastath ?></td>
                <td><?php echo $farmsoutheastafr ?></td>
                <td><?php echo $farmsoutheastaf ?></td>
                <td><?php echo $farmsoutheastas ?></td>
                <td><?php echo $farmsoutheastasv ?></td>

              </tr>
              </tr>


            </tbody>
          </table>
          <img src=<?php echo $dataimg ?> width="80%"></img>
          <img src=<?php echo $dataimg1 ?> width="80%"></img>
        </div>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</html>