<!--程式碼範例-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<title>客製化標記點工具</title>
<script type="text/javascript" src="https://api.tgos.tw/TGOS_API/tgos?ver=2&AppID=YTvBoU3+KlGNnQY0bVrcBWPZ6C7ghKJQsdnrE+DmFV/brGnGWAG39Q==&APIKey=cGEErDNy5yN/1fQ0vyTOZrghjE+jIU6ufThSqYm3hjzSaK8pYEgKKmkHcoqPE1UtE4fEanYtjmApOs5vhN+nxmCK7qBmwst6H58Hn7rqQDI7x0dHVM+h1Wchuje3Of208tsorv9DnMKDrL6/gu3ISGf/+J3cAuXvDJjIOrPMFxUaaFh2EiT5YxVwZioViKXseoUe6S9XFbA+I2qRj7ifJ8mCWkY0hwAiftTN+yzqflOZDkRsBbKbhW8GCjq9BTniO3WWghoHMOKLqr0P/RRMW10/lVQFh2X+Q8sNR22Ha6AhaLL51CaoVeWYDnyixQFgt5cblMNk9rNxbP5Hr1Cs17AB6kxnF1Is3ubau3d2Co7r99gmJSq0qQ==" charset="utf-8"></script>
<!--下載後請將yourID及yourkey取代為您申請所取得的APPID及APIKEY方能正確顯示服務-->
<script type="text/javascript"> 
	var pMap = null;
	var Markers = new Array();	//新增一個陣列, 準備存放使用者新增的Marker
	var detection;  //新增一個變數, 準備接收監聽器回傳值
	function InitWnd() {
		var pOMap = document.getElementById("TGMap");
		var mapOptions = {
			disableDefaultUI: true
		};
		pMap = new TGOS.TGOnlineMap(pOMap, TGOS.TGCoordSys.EPSG3826, mapOptions);	//宣告TGOnlineMap地圖物件並設定坐標系統
	}
	
	function AddMarker() {
		if(detection != "")
		{
			TGOS.TGEvent.removeListener(detection);  //如果監聽器已存在, 則移除監聽器
		}
		detection = TGOS.TGEvent.addListener(pMap, "click", function (tEvent) {	 //建立監聽器
			var pt = new TGOS.TGPoint(tEvent.point.x, tEvent.point.y);		//取得滑鼠點擊位置
			var url = "https://api.tgos.tw/TGOSMAPAPI/images/marker.png";	//取得滑鼠游標圖示URL
			var size = new TGOS.TGSize(40,40);	//取得圖示大小
			var anchor = new TGOS.TGPoint(20,40);	//取得錨點位移
            var message = document.getElementById("title").value + "<br> X坐標: " + pt.x + "<br> Y坐標: " + pt.y ;	//顯示標題+座標
			var markerOptions = { flat:false };

			var icon = new TGOS.TGImage(url, size, new TGOS.TGPoint(0, 0), anchor);
			var marker = new TGOS.TGMarker(pMap, pt, message, icon, markerOptions);
			
            Markers.push(marker);
		});
	}
	
	function RmvMarker() {
		for (i=0; i < Markers.length; i++) {
			Markers[i].setMap(null);
		}		
		Markers = [];
	}
</script>            
</head>
<body style="margin:0px" onLoad="InitWnd();">
	<div id="TGMap" style="width:100%; height:80%; border: 1px solid #C0C0C0;"></div><br>
	<div style="border:0px solid #000000; width:80%;">
        <br>
		農地命名:<input type="text" id="title" value="農地名稱" size="20"/>


		<input type="button" value="加入標記點" onClick="AddMarker();"/><input type="button" value="清除所有標記點" onClick="RmvMarker();"/>
	    <br>
    </div>	
</html>  
