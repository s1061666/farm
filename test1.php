<!--程式碼範例-->
<?php
session_start();
require "testc.php";
?>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>土地定位管理</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>

</head>

<body>
  <style>
  .navbar-light .navbar-brand {
    color: #ffffff;
  }

  .navbar-light .navbar-nav .nav-link {
    color: rgb(255, 255, 255);
  }

  #up {
    background-color: #2A6041 !important;
  }

  .carousel {
    perspective: 500px;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-image: url(image/bg1.jpg) !important;
    background-repeat: no-repeat;
    background-position: center 20%;
  }

  .slider {
    width: 50%;
    margin: 100px auto;
  }

  .slick-slide img {
    margin: auto;
  }

  /* Arrows */
  .slick-prev,
  .slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    border: none;
    background: transparent;
  }

  .slick-prev:hover,
  .slick-prev:focus,
  .slick-next:hover,
  .slick-next:focus {
    color: transparent;
    outline: none;
    background: transparent;
  }

  .slick-prev:hover:before,
  .slick-prev:focus:before,
  .slick-next:hover:before,
  .slick-next:focus:before {
    opacity: 1;
  }

  .slick-prev.slick-disabled:before,
  .slick-next.slick-disabled:before {
    opacity: .25;
  }

  .slick-prev:before,
  .slick-next:before {
    font-family: 'slick';
    font-size: 20px;
    line-height: 1;
    opacity: .75;
    color: white;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .slick-prev {
    left: -25px;
  }

  [dir='rtl'] .slick-prev {
    right: -25px;
    left: auto;
  }

  .slick-prev:before {
    content: '←';
  }

  [dir='rtl'] .slick-prev:before {
    content: '→';
  }

  .slick-next {
    right: -25px;
  }

  [dir='rtl'] .slick-next {
    right: auto;
    left: -25px;
  }

  .slick-next:before {
    content: '→';
  }

  [dir='rtl'] .slick-next:before {
    content: '←';
  }
  </style>

  <body class="text-center">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
        <a class="navbar-brand" href="first.html"><span class="h3 mx-1">農業風水師</span></a> <button
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
          class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span
            class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="main_2.html">首頁<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="woodtest.php">預約生產單</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="earthcontroller.php">木</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="test1.php">土</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.php">登入</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="register.php">註冊</a>
            </li>
          </ul>

        </div>
      </nav><br>
      <div class="jumbotron jumbotron-fluid"
        style="background-image:url('image/earth_bg.jpg'); background-size: 100%; background-repeat: no-repeat" ;>
        <p class="lead" style="color: red;font-weight:bold;">施肥提醒：</p>
      </div>

      <div class="container">
        <?php
header('Content-Type: text/html; charset=utf-8');
$con = mysqli_connect("127.0.0.1", "dluser", "dluser@515", "hacktonfarm") or die(mysqli_error($con));
//$con =  mysqli_connect($ip,$username,$password, $db) or die(mysqli_error($con));
print($ip);
mysqli_query($con, "SET CHARACTER SET UTF8");
$sql     = "SELECT * FROM ground";
$sql2    = "SELECT * FROM test_plant WHERE farmer_id='" . $_SESSION['log']['id'] . "'";
$result  = mysqli_query($con, $sql);
$result1 = mysqli_query($con, $sql2);
if (isset($_POST['farm_id'])) {
    $farmid = $_POST['farm_id'];
}
/*print("<form method='post'>");
print("<select name='farm_id' class='form-control'>");
while( $row1 = $result1->fetch_array(MYSQLI_ASSOC) ){
print("<option value='".$row1["id"]."'>".$row1["farm_name"]."</option>");
}
print("</select>");
print("<input type='submit' value='查看' class='btn btn-success'>");
print("</form>");*/
$sql3    = "SELECT * FROM test_plant WHERE id='" . $_SESSION["earthid"] . "'";
$result2 = mysqli_query($con, $sql3);
function getdistance($lng1, $lat1, $lng2, $lat2)
{
    //將角度轉為弧度
    $radLat1 = deg2rad($lat1);
    $radLat2 = deg2rad($lat2);
    $radLng1 = deg2rad($lng1);
    $radLng2 = deg2rad($lng2);
    $tmp1    = $radLat1 - $radLat2;
    $tmp2    = $radLng1 - $radLng2;
    //計算公式
    $d = 2 * asin(sqrt(pow(sin($tmp1 / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($tmp2 / 2), 2))) * 6378.137 * 1000;
    //單位公尺
    return intval($d);
}
while ($row2 = mysqli_fetch_array($result2)) {
    $lng1 = $row2["x"];
    $lat1 = $row2["y"];
    print($row2["cropname"]);
}
$adddistance = array();
echo "<table border=1>";
while ($row = mysqli_fetch_array($result)) {

    //echo "<tr>";
    //echo "<td>" .$row['id'] . "</td><td>" .$row['longitude'] . "</td><td>" . $row['latitude']. "</td><td>" . $row['COUNTYNAME']. "</td><td>" . $row['d']. "</td><td>" . $row['e']. "</td><td>" . $row['f']. "</td><td>" . $row['g']. "</td><td>" . $row['h']. "</td><td>" . $row['i']. "</td><td>" . $row['j']. "</td><td>" . $row['k']. "</td><td>" . $row['l']. "</td><td>" . $row['m']. "</td><td>" . $row['n']. "</td><td>" . $row['o']. "</td><td>" . $row['p']. "</td><td>" . $row['q']. "</td><td>" . $row['r']."</td>";
    $lng2     = $row['longitude'];
    $lat2     = $row['latitude'];
    $distance = getdistance($lng1, $lat1, $lng2, $lat2);
    //echo "<td>" .$distance."</td>";

    array_push($adddistance, $distance);
    // echo "</tr>";
}

echo "</table>";
$num     = array_keys($adddistance, min($adddistance));
$new_num = $num[0] + 1;
$sql2    = "SELECT * FROM ground Where id=$new_num";
$result2 = mysqli_query($con, $sql2);
while ($row = mysqli_fetch_array($result2)) {
    $groundface  = $row['groundface'];
    $firstground = $row['firstground'];

    array_push($adddistance, $distance);
}

if ($firstground == 1) {
    echo ("<br>土質:粗砂土，砂土<br>");
} elseif ($firstground == 2) {
    echo ("<br>土質:粗細砂土，壤質粗砂土，壤質砂土<br>");
} elseif ($firstground == 3) {
    echo ("<br>土質:粗壤質細砂土，粗砂質壤土，砂質壤土，細砂質壤土<br>");
} elseif ($firstground == 4) {
    echo ("<br>土質:粗極細砂土，壤質極細砂土，極細砂質壤土<br>");
} elseif ($firstground == 5) {
    echo ("<br>土質:粗坋土，坋質壤土<br>");
} elseif ($firstground == 6) {
    echo ("<br>土質:壤土<br>");
} elseif ($firstground == 7) {
    echo ("<br>土質:砂質粘壤土<br>");
} elseif ($firstground == 8) {
    echo ("<br>土質:粘壤土，坋質粘壤土<br>");
} elseif ($firstground == 9) {
    echo ("<br>土質:坋質粘土<br>");
} elseif ($firstground == 10) {
    echo ("<br>土質:粘土<br>");
} elseif ($firstground == 0) {
    echo ("目前該地區無資料");
} else {
    echo ("<br>土質:石礫<br>");
}
if ($firstground >= 1 || $firstground <= 2) {
    echo ("土壤偏砂，須留意根瘤線蟲之危害與防治<br>土壤偏砂，保肥力與保水力低，建議肥料與水分採少量多次");
}

if ($groundface < 4) {
    echo ("土壤酸鹼值偏低，建議酌量施用石灰，或適時補充鈣鎂");
} elseif ($groundface > 4 && $groundface <= 6) {
    echo ("土壤酸鹼值合適");
} elseif ($groundface == 0) {
    echo ("目前該地區無資料");
} else {
    echo ("土壤酸鹼值偏高，請勿施用石灰，並留意鈉含量是否偏高");
}
echo ("<br>表土層:" . $groundface . "<br>");
echo ("第一層:" . $firstground . "<br>");

/**
 * 計算兩點間的距離
 * $lng1 第一組地址的經度
 * $lat1 第一組地址的緯度
 * $lng2 第二組地址的經度
 * $lat2 第二組地址的緯度
 */

?>
      </div>
  </body>

</html>