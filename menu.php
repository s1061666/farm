<?php
  session_start();
  
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
  <a class="navbar-brand" href="first.html"><img alt="" class="d-inline-block align-top" height="30" src="images/H-logo.svg" width="30"> <span class="h3 mx-1">農業風水師</span></a> <button
aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span
class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item active"><a class="nav-link" href="first.html">首頁<span class="sr-only">(current)</span></a></li>
      <li class="nav-item"><a class="nav-link" href="reservation.php">預約生產單</a></li>
      <li class="nav-item"><a class="nav-link" href="money.php">金</a></li>
      <li class="nav-item"><a class="nav-link" href="wood.php">木</a></li>
      <li class="nav-item"><a class="nav-link" href="water.php">水</a></li>
      <li class="nav-item"><a class="nav-link" href="fire.php">火</a></li>
      <li class="nav-item"><a class="nav-link" href="earth.php">土</a></li>
      <?php  if (isset($_SESSION['log']) == ""){ ?>
      <li class="nav-item"><a class="nav-link" href="register.php">註冊</a></li>
      <li class="nav-item"><a class="nav-link" href="login.php">登入</a></li>
      <?php } ?>
    </ul>
  </div>
</nav>
